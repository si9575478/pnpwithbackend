const express=require('express');
const cors=require('cors');
const mongoose=require('mongoose');
const app=express();
//require('./db/conn.js');
const students=require('./models/register.js');
const port=process.env.PORT || 8000;
app.use(cors());
app.use(express.json());

//Post route
//The below code is used to insert the data into the data base
app.post('/insert',async(req,res)=>{

    const name=req.body.name;
    const email=req.body.email;
    const mobile=req.body.mobile;
    const address=req.body.address;
    const gender=req.body.gender;
    const message=req.body.message;

    const food= new students({name: name, email: email,mobile:mobile,address:address,gender:gender,message:message});
    try{
        await food.save();
        res.send('Data inserted into the database');
    }catch (err){
        console.log(err);

    }
});

//
//Get route
app.get('/read',async(req,res)=>{
    students.find({},(err,result)=>{
        if(err){
            res.send(err);
        }
        res.send(result);
    });
   
});

//
//Update Code  in this section i have the probleum
//Update Code  in this section i have the probleum
app.put('/update',async(req,res)=>{

    const newName=req.body.newName;
    const newEmail=req.body.newEmail;
    const newMobile=req.body.newMobile;
    const newAddress=req.body.newAddress;
    const newGender=req.body.newGender;
    const newMessage=req.body.newMessage;
    const id=req.body.id;
    
    try{
       await students.findById(id,(err,updatedStudent)=>{
        updatedStudent.name=newName,
        updatedStudent.email=newEmail,
        updatedStudent.mobile=newMobile,
        updatedStudent.address=newAddress,
        updatedStudent.gender=newGender,
        updatedStudent.message=newMessage,
        updatedStudent.save();
           res.send("update"); 
       })
    }catch (err){
        console.log(err);
    }
});

//
//
app.delete('/delete/:id',async(req,res)=>{
    const id=req.params.id;
    await students.findByIdAndRemove(id).exec();
    res.send("deleted");
});

//
app.listen(port,()=>{console.log(`The serrver is running at ${port}`)});
