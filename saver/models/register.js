require('../db/conn');


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = Schema({
  name: String,
 
 email:String,

 mobile:String,

 address:String,

 gender:String,

 message:String,

  status: {
    type: Boolean,
    default: false
  }
  
});

module.exports = mongoose.model('tasks', TaskSchema);
